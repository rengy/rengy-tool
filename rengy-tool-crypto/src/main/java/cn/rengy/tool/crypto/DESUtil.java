package cn.rengy.tool.crypto;


import org.apache.commons.codec.binary.Hex;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class DESUtil {
	  public static void main(String[] args) throws Exception {
	        //new DESUtil();
	        String ss=DESUtil.encrypt("12345678","123456781234567812345678","12345678");
	        System.out.print(ss);
	    }
	  /**
	   * content: 加密内容
	   * slatKey: 必须24位数
	   * vectorKey: 加密的向量，8位字符串
	   */
	  public static String encrypt(String content, String slatKey, String vectorKey) throws Exception {
		  if(slatKey.length()!=24){
			  throw new Exception("密码必须24位数！");
		  }
	      Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
	      SecretKey secretKey = new SecretKeySpec(slatKey.getBytes(), "DESede");
	      IvParameterSpec iv = new IvParameterSpec(vectorKey.getBytes());
	      cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);
	      byte[] encrypted = cipher.doFinal(content.getBytes());
		  return Hex.encodeHexString(encrypted);
	      //return Base64.toBase64String(encrypted);
	  }
}
