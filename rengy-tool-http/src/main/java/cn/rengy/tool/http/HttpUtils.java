/**
 * 
 */
package cn.rengy.tool.http;



import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.rengy.util.ExceptionPrintUtils;





public class HttpUtils {
	private static Logger logger = LoggerFactory.getLogger(HttpUtils.class);

	//public static final String CONTENT_TYPE_FORM ="application/x-www-form-urlencoded";
	//public static final int timeout = 10;
	/**
	 * post 请求
	 * 
	 * @param url
	 * @return
	 */
	public static String post(String url) {
		return post(url, "");
	}
	
	/**
	 * post请求
	 * @param url
	 * @param data
	 * @return
	 */
	public static String post(String url, String data){
		return httpPost(url, data);
	}
	public static String post(String url, String data,Charset charset){
		return httpPost(url, data,charset);
	}
	
	
	/**
	 * 发送http post请求
	 * @param url       url
	 * @param instream  post数据的字节流
	 * @return
	 */
	public static String post(String url, InputStream instream){
		try {
			HttpEntity entity = Request.Post(url)
					.bodyStream(instream,ContentType.create("text/html", StandardCharsets.UTF_8))
					.execute().returnResponse().getEntity();
			return entity != null ? EntityUtils.toString(entity,StandardCharsets.UTF_8) : null;
		} catch (Exception e) {
			logger.error("post请求异常，" + e.getMessage() + "\n post url:" + url);
			logger.error(ExceptionPrintUtils.getTrace(e));
		}
		return null;
	}
	
	/**
	 * get请求
	 * @param url
	 * @return
	 */
	public static String get(String url){
		return httpGet(url);
	}
	/**
	 * get请求
	 * @param url
	 * @return
	 */
	public static String get(String url,Charset charset){
		return httpGet(url,charset);
	}
	/**
	 * post 请求
	 * 
	 * @param url
	 * @param data
	 * @return
	 */
	private static String httpPost(String url, String data) {
		try {
			HttpEntity entity = Request.Post(url)
					.bodyString(data,ContentType.create("text/html", StandardCharsets.UTF_8))
					.execute().returnResponse().getEntity();
			return entity != null ? EntityUtils.toString(entity,StandardCharsets.UTF_8) : null;
		} catch (Exception e) {
			logger.error("post请求异常，" + e.getMessage() + "\n post url:" + url);
			logger.error(ExceptionPrintUtils.getTrace(e));
		}
		return null;
	}
	public static String httpPost(String url,String contentType, String data) {
		try {
			HttpEntity entity = Request.Post(url)
					.bodyString(data,ContentType.create(contentType, StandardCharsets.UTF_8))
					.execute().returnResponse().getEntity();
			return entity != null ? EntityUtils.toString(entity,StandardCharsets.UTF_8) : null;
		} catch (Exception e) {
			logger.error(ExceptionPrintUtils.getTrace(e));
		}
		return null;
	}
	private static String httpPost(String url, String data,Charset charset) {
		try {
			HttpEntity entity = Request.Post(url)
					.bodyString(data,ContentType.create("text/html", charset.name()))
					.execute().returnResponse().getEntity();
			return entity != null ? EntityUtils.toString(entity,charset) : null;
		} catch (Exception e) {
			logger.error("post请求异常，" + e.getMessage() + "\n post url:" + url);
			logger.error(ExceptionPrintUtils.getTrace(e));
		}
		return null;
	}
	
	/**
	 * 上传文件
	 * @param url    URL
	 * @param file   需要上传的文件
	 * @return
	 */
	public static String postFile(String url,File file){
		return postFile(url, null, file);
	}
	
	/**
	 * 上传文件
	 * @param url    URL
	 * @param name   文件的post参数名称
	 * @param file   上传的文件
	 * @return
	 */
	public static String postFile(String url,String name,File file){
		try {
			HttpEntity reqEntity = MultipartEntityBuilder.create().addBinaryBody(name, file).build();
			Request request = Request.Post(url);
			request.body(reqEntity);
			HttpEntity resEntity = request.execute().returnResponse().getEntity();
			return resEntity != null ? EntityUtils.toString(resEntity) : null;
		} catch (Exception e) {
			logger.error("postFile请求异常，" + e.getMessage() + "\n post url:" + url);
			logger.error(ExceptionPrintUtils.getTrace(e));
		}
		return null;
	}
	
	public static String postFile(String url,String name,byte[] data,String filename){
		try {
			HttpEntity reqEntity = MultipartEntityBuilder.create().addBinaryBody(name,data, ContentType.DEFAULT_BINARY, filename)
					.build();
			Request request = Request.Post(url);
			request.body(reqEntity);
			HttpEntity resEntity = request.execute().returnResponse().getEntity();
			return resEntity != null ? EntityUtils.toString(resEntity) : null;
		} catch (Exception e) {
			logger.error("postFile请求异常，" + e.getMessage() + "\n post url:" + url);
			logger.error(ExceptionPrintUtils.getTrace(e));
		}
		return null;
	}
	
	/**
	 * 下载文件
	 * @param url   URL
	 * @return      文件的二进制流，客户端使用outputStream输出为文件
	 */
	public static byte[] getFile(String url){
		try {
			Request request = Request.Get(url);
			HttpEntity resEntity = request.execute().returnResponse().getEntity();
			return EntityUtils.toByteArray(resEntity);
		} catch (Exception e) {
			logger.error("postFile请求异常，" + e.getMessage() + "\n post url:" + url);
			logger.error(ExceptionPrintUtils.getTrace(e));
		}
		return null;
	}

	public static HttpEntity getReturnEntity(String url){
		try {
			Request request = Request.Get(url);
			HttpEntity resEntity = request.execute().returnResponse().getEntity();
			return resEntity;
		} catch (Exception e) {
			logger.error("getFile请求异常，" + e.getMessage() + "\n get url:" + url);
			logger.error(ExceptionPrintUtils.getTrace(e));
		}
		return null;
	}
	/**
	 * 发送get请求
	 * 
	 * @param url
	 * @return
	 */
	private static String httpGet(String url) {
		try {
			HttpEntity entity = Request.Get(url).
					execute().returnResponse().getEntity();
			return entity != null ? EntityUtils.toString(entity, StandardCharsets.UTF_8) : null;
		} catch (Exception e) {
			logger.error("get请求异常，" + e.getMessage() + "\n get url:" + url);
			logger.error(ExceptionPrintUtils.getTrace(e));
		}
		return null;
	}
	
	private static String httpGet(String url,Charset charset) {
		try {
			HttpEntity entity = Request.Get(url).
					execute().returnResponse().getEntity();
			return entity != null ? EntityUtils.toString(entity,charset) : null;
		} catch (Exception e) {
			logger.error("get请求异常，" + e.getMessage() + "\n get url:" + url);
			logger.error(ExceptionPrintUtils.getTrace(e));
		}
		return null;
	}
	
	
}
