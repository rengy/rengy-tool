package cn.rengy.tool.qrcode;
 
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import com.google.zxing.WriterException;

import net.coobird.thumbnailator.Thumbnails;
/**
 * 无内边距二维码
 *
 */
public class QRCodeGenerator {
    
    
    private static void generateQRCodeImage(String text, int width, int height, File file) throws WriterException, IOException {
        
        
        BufferedImage image=com.google.zxing.client.j2se.MatrixToImageWriter.toBufferedImage(
        	    new com.google.zxing.qrcode.QRCodeWriter().encode(
        	    		text,com.google.zxing.BarcodeFormat.QR_CODE, width, height,
        	    com.google.common.collect.ImmutableMap.of(com.google.zxing.EncodeHintType.MARGIN,0)));
        Thumbnails.of(image).size(width, height) .toFile(file);
    }
    
    public static void main(String[] args) {
        try {
            generateQRCodeImage("This is my first QR Code", 80, 80, new File("D:/2.png"));
        } catch (WriterException e) {
            System.out.println("Could not generate QR Code, WriterException :: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Could not generate QR Code, IOException :: " + e.getMessage());
        }
        
    }
    
 
}