package cn.rengy.tool.image;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.commons.codec.binary.Hex;

public class ImageTypeUtils {

	public static final HashMap<String, String> fileTypes = new HashMap<String, String>();
	static{
		//images
		fileTypes.put("FFD8FF", "jpg");//FFD9结尾
		fileTypes.put("89504E47", "png");
		fileTypes.put("00000100", "ico");
		fileTypes.put("47494638", "gif");
		fileTypes.put("424D", "bmp");
		fileTypes.put("49492A00", "tif");
		
        /*mFileTypes.put("41433130", "dwg");// CAD  
        mFileTypes.put("38425053", "psd");
        mFileTypes.put("7B5C727466", "rtf");// 日记本  
        mFileTypes.put("3C3F786D6C", "xml");
        mFileTypes.put("68746D6C3E", "html");
        mFileTypes.put("44656C69766572792D646174653A", "eml"); // 邮件  
        mFileTypes.put("D0CF11E0", "doc");
        mFileTypes.put("5374616E64617264204A", "mdb");
        mFileTypes.put("252150532D41646F6265", "ps");
        mFileTypes.put("255044462D312E", "pdf");
        mFileTypes.put("504B0304", "docx");
        mFileTypes.put("52617221", "rar");
        mFileTypes.put("57415645", "wav");
        mFileTypes.put("41564920", "avi");
        mFileTypes.put("2E524D46", "rm");
        mFileTypes.put("000001BA", "mpg");
        mFileTypes.put("000001B3", "mpg");
        mFileTypes.put("6D6F6F76", "mov");
        mFileTypes.put("3026B2758E66CF11", "asf");
        mFileTypes.put("4D546864", "mid");
        mFileTypes.put("1F8B08", "gz");
        mFileTypes.put("4D5A9000", "exe/dll");
        mFileTypes.put("75736167", "txt");*/
	}
	/**
	 * 通过文件头解析文件后缀
	 * @param file
	 * @return
	 */
	public static String getFilePostfix(File file){
		String fileType="unknown";
		FileInputStream is = null;
        try {
            is = new FileInputStream(file);  
            byte[] b = new byte[4];  
            is.read(b);
            String fileHeader = Hex.encodeHexString(b).toUpperCase();
            fileType=fileHeader;
            //System.out.println(value);
            Iterator<Entry<String, String>> ite = fileTypes.entrySet().iterator();  
            while (ite.hasNext()) {
            	Entry<String, String> entry = ite.next();
            	String key = entry.getKey();
                String value = entry.getValue();
                //System.out.println("::"+value);
                if(fileHeader.startsWith(key)){
                	fileType=value;
                	break;
                }
            }
        } catch (Exception e) {  
        } finally {
            if (is!=null) {  
                try {  
                    is.close();  
                } catch (IOException e) {  
                }  
            }  
        }
        return fileType;
	}
	
	public static String getFilePostfix(byte[] head){
		String fileType="unknown";
        String fileHeader = Hex.encodeHexString(head).toUpperCase();
        fileType=fileHeader;
        //System.out.println(value);
        Iterator<Entry<String, String>> ite = fileTypes.entrySet().iterator();  
        while (ite.hasNext()){
        	Entry<String, String> entry = ite.next();
        	String key = entry.getKey();
            String value = entry.getValue();
            //System.out.println("::"+value);
            if(fileHeader.startsWith(key)){
            	fileType=value;
            	break;
            }
        }
        return fileType;
	}
	
	
	public static void main(String args[]){
		 String s=getFilePostfix(new File
				 ("D:/git/lvzhi/src/main/webapp/attached/QRCODE/12"));
		 System.out.println(s);
		//
	}
}
