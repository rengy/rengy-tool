package cn.rengy.tool.excel;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
public class ExcelUtils {

	/**
	 * HSSFWorkbook 2003以下版本
	 * @param outputStream
	 * @param dataList
	 */
	public static void export2003(OutputStream outputStream,List<Map<String,Object>> dataList){
		if(dataList!=null&&dataList.size()>0){
			Map<String,Object> cellMap=dataList.get(0);
			//创建工作薄对象
	        HSSFWorkbook wb = new HSSFWorkbook();
	        int dataLength=dataList.size();
	        int sheetMaxLength=65535;
	        double d=(double)dataLength/sheetMaxLength;
	        int sheetCount=(int)Math.ceil(d);
	        for(int k=0;k<sheetCount;k++){
	            HSSFSheet sheet =  wb.createSheet();//创建sheet
                int rowStart=k*sheetMaxLength;
           	 	int rowCount=(rowStart+sheetMaxLength)>dataLength?dataLength:(rowStart+sheetMaxLength);
	           	for (int i = rowStart,rowIndex = 0; i < rowCount; i++) {//写sheet
	           		Map<String,Object> data=dataList.get(i);
	           		HSSFRow row = sheet.createRow(rowIndex++);//创建行
	           		Set<String> keySet=cellMap.keySet();
	           		Iterator<String> ite=keySet.iterator();
	           		int cellIndex=0;
	           		while(ite.hasNext()){
	           			HSSFCell cell = row.createCell(cellIndex++);
	           			Object value=data.get(ite.next());
	        	        cell.setCellValue(value==null?"":value.toString());
	           		}
	           	}
	        }
	        
	        try {
				wb.write(outputStream);
			} catch (IOException e) {
			}finally{
				try {
					outputStream.flush();
					outputStream.close();
				} catch (IOException e) {
				}
				try {
					wb.close();
				} catch (IOException e) {
				}
				
			}
		}
	}
	public static void export2007(OutputStream outputStream,List<Map<String,Object>> dataList){
		 if(dataList!=null&&dataList.size()>0){
				Map<String,Object> cellMap=dataList.get(0);
				//创建工作薄对象
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = workbook.createSheet();
		        int size=dataList.size();
		        for (int i = 0;i < size; i++) {//写sheet
	           		Map<String,Object> data=dataList.get(i);
	           		SXSSFRow row = sheet.createRow(i);
	           		Set<String> keySet=cellMap.keySet();
	           		Iterator<String> ite=keySet.iterator();
	           		int cellIndex=0;
	           		while(ite.hasNext()){
	           			SXSSFCell cell = row.createCell(cellIndex++);
	           			Object value=data.get(ite.next());
	        	        cell.setCellValue(value==null?"":value.toString());
	           		}
	           	}
		        
		        try {
		        	workbook.write(outputStream);
				} catch (IOException e) {
				}finally{
					dataList.clear();
					try {
						outputStream.flush();
						outputStream.close();
					} catch (IOException e) {
					}
					try {
						workbook.close();
					} catch (IOException e) {
					}
				}
			}
	}
	
	public static void main(String args[]){
		List<Map<String,Object>> dataList =new ArrayList<Map<String,Object>>();
		for(int i=0;i<11;i++){
			Map<String,Object> m=new HashMap<String,Object>();
			m.put("a", "xxxxxxxxxxxxx");
			m.put("b", "xxxxx");
			m.put("c", "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
			dataList.add(m);
		}
		long starttime=System.currentTimeMillis();
		try {
			export2007(new FileOutputStream("D:/1.xlsx"), dataList);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		long endtime=System.currentTimeMillis();
		System.out.println("耗时："+(endtime-starttime)/1000);
	}
}
