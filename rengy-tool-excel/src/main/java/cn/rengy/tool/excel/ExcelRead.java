package cn.rengy.tool.excel;

import cn.rengy.tool.core.util._DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ExcelRead {
    public interface RowHandler{
        void handleRow(List<List<String>> rowList);
    }

    private static class DefaultRowHandler implements RowHandler {

        @Override
        public void handleRow(List<List<String>> rowList) {
            for(List<String> row:rowList){
                for(String cellValue:row){
                    System.out.print(cellValue);
                    System.out.print(",");
                }
                System.out.println();
            }
        }
    }
    public static void main(String[] args) {
        //File file1=new File("D:\\国网电力\\文档\\新建文件夹(1)\\新建文件夹\\电源项目规划前期建设情况统计表 (3).xlsx");
        //ExcelRead.out(new DefaultRowHandler(),file1, 6, 5,0);
        File file2=new File("D:\\国网电力\\文档\\比较2\\电源项目规划前期建设情况统计表（试行）-全口径-汇总(2).xlsx");
        System.out.println("====================================================");
        ExcelRead.out(new DefaultRowHandler(),file2, 5, 4,2);
    }
    public static void out(RowHandler rowHandler, File excelFile, int dataStartRow, int maxCellRow, int sheetIndex){
        FileInputStream input= null;
        XSSFWorkbook workbook=null;
        try {
            input = new FileInputStream(excelFile);
            workbook = new XSSFWorkbook(input);
            XSSFSheet sheet = workbook.getSheetAt(sheetIndex);
            log.debug("读取SheetName:{}",sheet.getSheetName());
            //最大列数，读取第6行
            int maxCell=0;
            XSSFRow row=sheet.getRow(maxCellRow);
            XSSFCell headerCell=null;
            do{
                maxCell++;
            }while (haveNextCell(row, maxCell));
            //System.out.println(maxCell);
            //从第7行开始读取数据
            String cell0Value=null;
            int startRow=dataStartRow;
            List<List<String>> rowList=new ArrayList<>();
            do{
                row=sheet.getRow(startRow++);
                List<String> rowData=new ArrayList<>();
                //System.out.print(startRow);
                //System.out.print("::");
                for(int i=0;i<maxCell;i++){
                    String cellValue=getValue(row,i);
                    rowData.add(cellValue);
                    //System.out.print(cellValue);
                    //System.out.print(",");
                }
                rowList.add(rowData);
                //System.out.println();
            }while (haveNextRow( sheet,startRow));
            rowHandler.handleRow(rowList);
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                workbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getValue(XSSFRow row,int colNum ){
        XSSFCell cell = row.getCell(colNum);
        if (cell==null){
            return "";
        }
        String cellValue = "";
        CellType cellType=cell.getCellType();
        if (cellType== CellType.NUMERIC) {
            if(DateUtil.isCellDateFormatted(cell)){
                cellValue= _DateUtils.formatDate(cell.getDateCellValue(),null,"yyyy-MM-dd");
            }else{
                cellValue=String.valueOf(cell.getNumericCellValue());
            }
        } else if (cellType == CellType.STRING) {
            cellValue = cell.getStringCellValue();
        }else if (cellType == CellType.BLANK) {
            cellValue = "";
        }else if (cellType == CellType.FORMULA) {
            cellValue=String.valueOf(cell.getNumericCellValue());
        } else {
            throw new RuntimeException("未处理的cell类型"+cellType.toString());
        }
        return cellValue;
    }


    private static boolean haveNextRow(XSSFSheet sheet,int rowIndex){
        XSSFRow row=sheet.getRow(rowIndex);

        if(row!=null){
            String cell0Value=getValue( row,0);
            return StringUtils.isNotBlank(cell0Value);
        }
        return false;
    }

    private static boolean haveNextCell(XSSFRow row,int CellIndex){
        XSSFCell cell=row.getCell(CellIndex);
        return cell!=null;
    }
}
