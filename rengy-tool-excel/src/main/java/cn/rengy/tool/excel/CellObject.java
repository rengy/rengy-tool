package cn.rengy.tool.excel;

import lombok.Getter;
import lombok.Setter;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.util.List;

@Getter
@Setter
public class CellObject {


    private String text;

    private int rowNum;//行数

    private int halfHeight;

    private List<String> imageList;

    private CellStyle cellStyle;

    private Font font;

    private SXSSFWorkbook workbook;



    public CellObject(SXSSFWorkbook workbook){
        this.workbook=workbook;
        cellStyle=  workbook.createCellStyle();//单元格样式

        font = workbook.createFont();
        font.setFontName("宋体");
        cellStyle.setFont(font);
        setBorder();
    }

    public static CellObject instance(SXSSFWorkbook workbook){
        return new CellObject(workbook);
    }

    public CellObject setFontSize(int fontSize){
        //行高就是 fontSize*20,
        font.setFontHeightInPoints((short) fontSize);//设置字体大小
        return this;
    }

    /**
     * 设置正文样式
     * @return
     */
    public CellObject setBodyStyle(){
        setFontSize(11).setAlignmentLEFT().setVerticalAlignmentCENTER().setWrapText();
        return this;
    }

    /**
     * 设置label样式
     * @return
     */
    public CellObject setLabelStyle(){
        setFontSize(11).setBold()
                .setAlignmentCENTER().setVerticalAlignmentCENTER()
        ;
        return this;
    }

    public CellObject setBold(){
        font.setBold(true);
        font.getFontHeight();
        return this;
    }

    /**
     * 设置自动换行
     * @return
     */
    public CellObject setWrapText(){
        cellStyle.setWrapText(true);//自动换行
        return this;
    }

    public CellObject setBorder(){
        cellStyle.setBorderBottom(BorderStyle.THIN); //下边框
        cellStyle.setBorderLeft(BorderStyle.THIN);//左边框
        cellStyle.setBorderTop(BorderStyle.THIN);//上边框
        cellStyle.setBorderRight(BorderStyle.THIN);//右边框
        return this;
    }
    public CellObject setBorderLeftNone(){
        cellStyle.setBorderLeft(BorderStyle.NONE);//左边框
        return this;
    }
    /**
     * 水平居中
     * @return
     */
    public CellObject setAlignmentCENTER(){
        cellStyle.setAlignment(HorizontalAlignment.CENTER);//水平居中
        return this;
    }

    /**
     * 水平居左
     * @return
     */
    public CellObject setAlignmentLEFT(){
        cellStyle.setAlignment(HorizontalAlignment.LEFT);//水平居左
        return this;
    }

    /**
     * 水平居右
     * @return
     */
    public CellObject setAlignmentRIGHT(){
        cellStyle.setAlignment(HorizontalAlignment.RIGHT);//水平居右
        return this;
    }
    /**
     * 垂直居中
     * @return
     */
    public CellObject setVerticalAlignmentCENTER(){
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);//垂直居中
        return this;
    }
}
