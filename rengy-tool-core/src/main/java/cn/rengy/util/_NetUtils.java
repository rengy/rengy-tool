package cn.rengy.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class _NetUtils {
	private static final Logger LOGGER = LoggerFactory.getLogger(_NetUtils.class);


	/**
	 *
	 * 使用UTF-8进行URL编码
	 *
	 * @param str
	 * @return
	 */
	public static String urlEncode(String str) {
		//apache commons URLCodec?
        String result = str;
        try {
        	if(str!=null){
        		result = URLEncoder.encode(str, StandardCharsets.UTF_8.name());
        		result = result.replaceAll("\\+", "%20");
        	}
        } catch (UnsupportedEncodingException e) {
        }
        return result;
    }
	public static String urlDecode(String url) {
		String result = url;
		try {
			result= URLDecoder.decode(url, StandardCharsets.UTF_8.name());
		} catch (UnsupportedEncodingException e) {
		}
		return result;
    }
	
}
