package cn.rengy.tool.core.util;

public class RandomString {
	public static String getRandomString(int length) {
	    //随机字符串的随机字符库
	    String KeyString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~!@$%^&*()_+=-?;:/\\<>,.{}[]'`";
	    StringBuffer sb = new StringBuffer();
	    int len = KeyString.length();
	    for (int i = 0; i < length; i++) {
	       sb.append(KeyString.charAt((int) Math.round(Math.random() * (len - 1))));
	    }
	    return sb.toString();
	}
	public static final int MYSECLENGTH=128;
	public static final int MYFISTLENGTH=12;
	public static final int MYFISTNUM=10000;
	//128随机串 中间加入#X#（0-9）
	public static String getRandomStringForBox(char x) {
		int head=(int)(Math.random() * MYSECLENGTH);
		String KeyString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~!@$%^&*()_+=-?;:/\\<>,.{}[]'`";
	    StringBuffer sb = new StringBuffer();
	    int len = KeyString.length();
	    for (int i = 0; i < MYSECLENGTH; i++) {
	    	if(i==head){
			 	sb.append('#');
				sb.append(x);
				sb.append('#');
		 	}
		 	sb.append(KeyString.charAt((int) Math.round(Math.random() * (len - 1))));
	    }
	    return sb.toString();
	}
	
	public static String getRandomStringForFist(int fist) {
		String KeyString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~!@$%^&*()_+=-?;:/\\<>,.{}[]'`#";
		StringBuffer sb = new StringBuffer();
	    int len = KeyString.length();
	    for (int i = 0; i < MYFISTLENGTH; i++) {
		 	sb.append(KeyString.charAt((int) Math.round(Math.random() * (len - 1))));
	    }
	    int num=(int)(Math.random() * MYFISTNUM);
	    num=num*3;
	    sb.append(""+num);
	    sb.append(""+fist);
	    return sb.toString();
	}
}
