package cn.rengy.tool.core.util;

import java.math.BigDecimal;


public class NumberUtils{
	
	public static int toInt(final Object object, final int defaultValue){
        if(object == null) {
            return defaultValue;
        }
        if(object instanceof Number){
        	return ((Number)object).intValue();
        }
        try {
            return Integer.parseInt(object.toString());
        } catch (NumberFormatException nfe) {
            return defaultValue;
        }
    }
	public static int toInt(final Object object) throws Exception{
        if(object == null) {
            throw new NullPointerException("参数为空");
        }
        if(object instanceof Number){
        	return ((Number)object).intValue();
        }
        return Integer.parseInt(object.toString());
    }
	public static Long toLong(final Object object, final Long defaultValue){
        if (object == null) {
            return defaultValue;
        }
        if(object instanceof Number){
        	return ((Number)object).longValue();
        }
        try {
            return Long.parseLong(object.toString());
        } catch (NumberFormatException nfe) {
            return defaultValue;
        }
    }
	public static Long toLong(final Object object){
        if (object == null) {
        	throw new NullPointerException("参数为空");
        }
        if(object instanceof Number){
        	return ((Number)object).longValue();
        }
        return Long.parseLong(object.toString());
    }
	public static Double toDouble(final Object object, final double defaultValue){
        if (object == null) {
            return defaultValue;
        }
        if(object instanceof Number){
        	return ((Number)object).doubleValue();
        }
        try {
            return Double.parseDouble(object.toString());
        } catch (NumberFormatException nfe) {
            return defaultValue;
        }
    }
	public static BigDecimal toBigDecimal(final Object object,BigDecimal defaultValue){
        if (object == null) {
            return defaultValue;
        }
        if(object instanceof BigDecimal){
        	return (BigDecimal)object;
        }
        try {
        	return new BigDecimal(object.toString());
        } catch (Exception nfe) {
        	return defaultValue;
        }
    }
	
}
