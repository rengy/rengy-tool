package cn.rengy.tool.core.util;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Slf4j
public class ImageReplaceUtils {

	public interface SrcReplace {
		/**
		 * 替换src的值
		 * 微信公众号图文消息的具体内容中，将过滤外部的图片链接
		 * 所以检查src是否以http://mmbiz.qpic.cn/开头
		 * imgSrc.startsWith("http://mmbiz.qpic.cn/")
		 * @param imgSrc
		 * @return 新的图片地址
		 */
		String replace(String imgSrc);
	}
	/**
	 * 替换的时候两种思路
	 * 1.通过捕获3个子表达式(<img\\s+[^>]*src\\s*=\\s*\\\")(.*?)(\\\"[^>]*>)  part1+src+part3 
	 * 2.通过捕获1个子表达式<img\\s+[^>]*src\\s*=\\s*\\\"(.*?)\\\"[^>]*> ,通过replacae替换旧的src
	 * 代码采用的是第1种思路
	 * 正则解析：
     * 1.<img     ->   匹配<img
     * 2.\\s+     ->   一次或多次匹配空白符（包括空格、制表符、换页符等。与 [ \f\n\r\t\v] 等效。）
     * 3.[^>]*    ->   匹配除>的任意字符
     * 4.匹配src
     * 5.0次或多次匹配空白符
     * 6.=        ->   匹配 =
     * 7.\\s*     ->   0次或多次匹配空白符
     * 8.\\\"     ->   匹配"
     * 9.(.*?)    ->   匹配除\r\n的任意字符并捕获此表达式src的值
     * 10.\\\"    ->   匹配"
     * 11.[^>]*   ->   0次或多次匹配除>的任意字符
     * 12.>       ->   匹配>
     * **/
	private static final String IMG_REGEX = "(<img\\s+[^>]*src\\s*=\\s*\\\")(.*?)(\\\"[^>]*>)";
	
	public static String replace(SrcReplace srcReplace, String content){
		StringBuffer sb = new StringBuffer();
        Pattern p_image = Pattern.compile(IMG_REGEX);
        Matcher m_image = p_image.matcher(content);
        while (m_image.find()){
        	//获取整个img:m_image.group(0)
            String src = m_image.group(2);//src的值
            String newSrc=srcReplace.replace(src);//替换src
            String imgPart1 = m_image.group(1);//开头字符
            String imgPart3 = m_image.group(3);//结尾字符
            m_image.appendReplacement(sb,imgPart1+newSrc+imgPart3);  
        }
        m_image.appendTail(sb);
		return sb.toString();
	}
}
