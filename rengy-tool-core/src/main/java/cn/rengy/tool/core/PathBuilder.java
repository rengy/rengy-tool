package cn.rengy.tool.core;


public class PathBuilder {

	protected StringBuilder pathBuffer;
	
	public PathBuilder(){
		pathBuffer=new StringBuilder();
	}
	public PathBuilder(String path){
		pathBuffer=new StringBuilder(path);
		
	}
	
	public PathBuilder append(String path){
		if(path!=null&&path.length()>0){
			if(pathBuffer.length()>0){
				if(pathBuffer.charAt(pathBuffer.length()-1)=='/') {//builder以/结尾
					if(path.charAt(0)=='/'){
						pathBuffer.deleteCharAt(this.pathBuffer.length()-1);
					}
				}else {
					if(path.charAt(0)!='/'){
						this.pathBuffer.append('/');
					}
				}
			}
			pathBuffer.append(path);
		}
		return this;
	}
	
	public String toString(){
		return this.pathBuffer.toString();
	}
	
	public static void main(String[] args) {
		PathBuilder p=new PathBuilder("http://xx.com/");
		p.append("/");
		p.append("/1/");
		p.append("/2/");
		p.append("/2/");
		p.append("/5/");
		System.out.println(p.toString());
	}
}
