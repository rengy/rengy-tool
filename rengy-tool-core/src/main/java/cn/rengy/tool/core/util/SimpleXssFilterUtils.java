package cn.rengy.tool.core.util;

public class SimpleXssFilterUtils {
	
	public static String clean(String input) {
        if (input == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder(input.length()+16);
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            switch (c) {
            case '<':
            	sb.append("&lt;");
                break;
            case '>':
            	sb.append("&gt;");
                break;
            case '&':
            	sb.append("&amp;");
                break;
            default:
            	sb.append(c);
            }
        }
        return sb.toString();
    }
}
