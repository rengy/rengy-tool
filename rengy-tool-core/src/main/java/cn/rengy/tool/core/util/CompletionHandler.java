
package cn.rengy.tool.core.util;


public interface CompletionHandler<A> {

    
    void completed( A attachment);

    void success(A attachment);
    
    void failed(Throwable exc, A attachment);
}
