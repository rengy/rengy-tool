package cn.rengy.tool.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;


public class _DateUtils{
	/**
	 * 
	 * @param source
	 * @param fromPattern
	 * @param toPattern
	 * @return
	 */
	public static String formatDate(Object source,String fromPattern,String toPattern){
		String newValue=null;
		if(source!=null){
			SimpleDateFormat formatter = new SimpleDateFormat(toPattern);
			if(source instanceof java.util.Date){
				newValue=formatter.format(source);
			}else if(source.getClass()==String.class){
				try {
					Date date =  DateUtils.parseDate(source.toString(), fromPattern);
					newValue=formatter.format(date);
				} catch (ParseException e) {
				}
			}else if(source.getClass()==Long.class){
				newValue=formatter.format(new Date((Long)source));
			}
		}
        return newValue;
	}
	
	public static Date parseDate(String source,String... parsePatterns){
		Date date=null;
		if(StringUtils.isBlank(source)){
			return null;
		}
		if(parsePatterns==null){
			parsePatterns=new String[1];
			parsePatterns[0] = "yyyy-MM-dd HH:mm:ss";
		}
		try {
			date = DateUtils.parseDate(source, null, parsePatterns);
		} catch (Exception e) {}
		return date;
	}
	
	public static Date getFirstDayOfMonth() {
		LocalDate today = LocalDate.now();
        //本月的第一天
        LocalDate firstday = LocalDate.of(today.getYear(),today.getMonth(),1);
        return Date.from(firstday.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}
	
	public static Date getLastDayOfMonth() {
		LocalDate today = LocalDate.now();
        //本月的第一天
		LocalDate lastDay =today.with(TemporalAdjusters.lastDayOfMonth());
        return Date.from(lastDay.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}
	
	public static String getYearMonth() {
    	StringBuilder sb=new StringBuilder();
    	//获取当前时间
        LocalDateTime currentDate = LocalDateTime.now();
        //获取年份
        int year = currentDate.getYear();
        sb.append(year);
        //获取月份
        int month = currentDate.getMonthValue();
        sb.append("-").append(month);
        return sb.toString();
    }
    
    public static String getYearWeek() {
    	StringBuilder sb=new StringBuilder();
    	//获取当前时间
        LocalDateTime currentDate = LocalDateTime.now();
        
        //获取年份
        int year = currentDate.getYear();
        sb.append(year);
        //获取当前周
        WeekFields weekFields = WeekFields.of(DayOfWeek.MONDAY,1);
        int weeks = currentDate.get(weekFields.weekOfYear());
        sb.append("-").append(weeks);
        return sb.toString();
    }
    
	public static Date getStartDayOfWeek() {
		LocalDate today = LocalDate.now();
        TemporalField fieldIso = WeekFields.of(DayOfWeek.MONDAY, 1).dayOfWeek();
        LocalDate localDate = LocalDate.from(today);
        localDate = localDate.with(fieldIso, 1);
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	public static Date getEndDayOfWeek() {
		LocalDate today = LocalDate.now();
		TemporalField fieldIso = WeekFields.of(DayOfWeek.MONDAY, 1).dayOfWeek();
        LocalDate localDate = LocalDate.from(today);
        localDate = localDate.with(fieldIso, 7);
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}
	// Scales as constants
	private static final long NANO_SCALE   = 1L;//纳秒
	private static final long MICRO_SCALE  = 1000L * NANO_SCALE;//微秒
	private static final long MILLI_SCALE  = 1000L * MICRO_SCALE;//毫秒
	private static final long SECOND_SCALE = 1000L * MILLI_SCALE;//秒
	private static final long MINUTE_SCALE = 60L * SECOND_SCALE;
	private static final long HOUR_SCALE   = 60L * MINUTE_SCALE;
	private static final long DAY_SCALE    = 24L * HOUR_SCALE;

	public static String spendFormat(long durationNano){
		StringBuilder sb=new StringBuilder();
		long seconds = durationNano /  SECOND_SCALE;
		if(seconds>0){
			durationNano = durationNano - (seconds * SECOND_SCALE);
			sb.append(seconds).append("s");
		}
		long milli= durationNano /  MILLI_SCALE;
		if(milli>0){
			sb.append(milli).append(".");
			durationNano = durationNano - (milli * MILLI_SCALE);
			long micro= durationNano /  MICRO_SCALE;
			sb.append(micro);
			sb.append("ms");
		}
		return sb.toString();
	}
}
