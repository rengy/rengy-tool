package cn.rengy.tool.core.util;

public class ByteUtils {
	/**
	 * int转byte数组
	 * @param iSource
	 * @param iArrayLen
	 * @return
	 */
	public static byte[] int2ByteArray(int iSource, int iArrayLen) {
	    byte[] bLocalArr = new byte[iArrayLen];
	    for (int i = 0; (i < 4) && (i < iArrayLen); i++) {
	        bLocalArr[i] = (byte) (iSource >> 8 * i & 0xFF);
	    }
	    return bLocalArr;
	}
	/**
	 * byte数组转int
	 * @param bRefArr
	 * @return
	 */
	public static int byteArray2Int(byte[] bRefArr) {
	    int iOutcome = 0;
	    byte bLoop;

	    for (int i = 0; i < bRefArr.length; i++) {
	        bLoop = bRefArr[i];
	        iOutcome += (bLoop & 0xFF) << (8 * i);
	    }
	    return iOutcome;
	}
	
	public static int byte2Int(byte ibyte) {
	    return (ibyte & 0xFF);
	}
}
