package cn.rengy.tool.core.util;

import cn.rengy.tool.core.util.Updated;

import java.util.ArrayList;
import java.util.List;


public class _Utils{
	
	
	/**
	 * 注意：curList和selectedId的值都会改变
	 * @param curList 当前持久化的id
	 * @param selectedId 客户端请求的id
	 * @return
	 */
	public static Updated getUpdate(List<Long> curList, List<Long> selectedId){
		Updated updated=new Updated();
		
		if(selectedId!=null&&selectedId.size()>0) {//已选择信息
			if(curList!=null&&curList.size()>0) {
				List<Long> insert=new ArrayList<Long>();
				for(int i=0;i<selectedId.size();i++) {
					Long selectId= selectedId.get(i);
					boolean found=false;
					for(int j=0;j<curList.size();j++) {
						Long curId=curList.get(j);
						if(selectId.equals(curId)) {
							found=true;
							curList.remove(j);
							break;
						}
					}
					if(!found) {
						insert.add(selectId);
					}
				}
				updated.setInsert(insert);
				//
				if(curList.size()>0) {
					updated.setDelete(curList);
				}
			}else {
				//当前选择为空
				updated.setInsert(selectedId);
			}
			
		}else {//已选择为空
			//持久化id全部删除
			updated.setDelete(curList);
		}
		return updated;
	}
	
	
}
