package cn.rengy.tool.core.util;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Updated {

	private List<Long> delete;
	private List<Long> insert;
}
