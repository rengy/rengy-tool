package cn.rengy.tool.core.util;

/**
 * 32进制去掉了 0O1l 4个字符，还能使用高效位运算
 * 不包含url特殊转义字符
 * 64进制转换 鸡肋 因为 mysql _是通配符，大小写在不区分大小写时有问题，
 * （java的Long.toString最大支持36进制(26个字母a-z加上0-9)）
 * @author rengy
 *
 */
public class _LongUtil {
	// 32位  =2的5次方，可以使用高效位运算
	private static final String chars = "23456789abcdefghijkmnpqrstuvwxyz";
	private static final int radix=32;//chars.length()
	private static final int radix1=31;
	static final int n=5;
	
	public static long valueOf(String str) {
		return valueOf32(str);
	}
	public static long valueOf32(String str) {
        long num = 0;
        int index = 0;
        for (int i = 0; i < str.length(); i++) {
            //查找字符的索引位置
            index = chars.indexOf(str.charAt(i));
            //索引位置代表字符的数值
            num += (long) (index * (Math.pow(radix, str.length() - i - 1)));
        }
        return num;
    }
	
	public static String toString(long i) {
		return toString32(i);
	}
	public static String toString32(long i) {
		if(i<0) {
			throw new IllegalArgumentException("不支持负数的进制转换");
		}
		/*if (radix < 2)
            radix = 2;
		if(radix > chars.length()) {
			radix = chars.length();
		}*/
		int charLength=15;//long最大值转成62进制后是11位,94进制是10位,36是13
        char[] buf = new char[charLength];
        int charPos = charLength-1;
        while (i >= radix){
            //buf[charPos--] = chars.charAt((int)(i % radix));
        	//高效率模运算 A%B，当满足B=2n  等价于 A&(B-1)
        	buf[charPos--] = chars.charAt((int)(i & radix1));
        	//高效除法运算n/(2^k) 等价于    n>>k 
        	i = i >>n;//
            //i = i / radix;
        }
        // 乘法运算转成位运算 方法：n<<k  等价于   n*(2^k) 左移一位相当于乘以2。
        
        buf[charPos] = chars.charAt((int)(i));
        return new String(buf, charPos, (charLength - charPos));
    }
	public static String toString36(long i){
		return Long.toString(i,Character.MAX_RADIX);
	}
	
	public static long valueOf36(String str){
		return Long.valueOf(str, Character.MAX_RADIX);
	}
	
	public static void main(String[] args) {
		long startTime=System.nanoTime();
//		for(int i=0;i<10000;i++) {
//			//Long.toString(i,Character.MAX_RADIX);//耗时:1658600  耗时:12491900
//			_LongUtil.toString32(i);//               耗时:1067600  耗时:7467200
//		}
		long endTime=System.nanoTime();
		//System.out.println("耗时:"+(endTime-startTime));
		String ss=_LongUtil.toString(0L);
		System.out.println(ss);
	}
}
