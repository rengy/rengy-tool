package cn.rengy.tool.core.util;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;



public class _NumberUtils {
	/*public static void main(String args[]) {
		int a=toInt(1, 0);
		System.out.println(a);
	}*/
	
	public static String bigDecimalToString(BigDecimal value) {
		if(value==null) {
			return null;
		}
		return value.toPlainString();
	}
	public static int toInt(final Object object, final int defaultValue){
        if(object == null) {
            return defaultValue;
        }
        if(object instanceof Number){
        	return ((Number)object).intValue();
        }
        try {
            return Integer.parseInt(object.toString());
        } catch (NumberFormatException nfe) {
            return defaultValue;
        }
    }
	public static int toInt(final Object object){
        if(object == null) {
            throw new NullPointerException("参数为空");
        }
        if(object instanceof Number){
        	return ((Number)object).intValue();
        }
        return Integer.parseInt(object.toString());
    }
	public static Long toLong(final Object object, final Long defaultValue){
        if (object == null) {
            return defaultValue;
        }
        if(object instanceof Number){
        	return ((Number)object).longValue();
        }
        try {
            return Long.parseLong(object.toString());
        } catch (NumberFormatException nfe) {
            return defaultValue;
        }
    }
	
	public static long toLongValue(final Object object){
		if (object == null) {
            return 0l;
        }
        if(object instanceof Number){
        	return ((Number)object).longValue();
        }
        try {
            return Long.parseLong(object.toString());
        } catch (NumberFormatException nfe) {
            return 0l;
        }
    }
	public static Long toLong(final Object object){
        if (object == null) {
        	return null;
        }
        if(object instanceof Number){
        	return ((Number)object).longValue();
        }
        try {
            return Long.parseLong(object.toString());
        } catch (NumberFormatException nfe) {
            return null;
        }
    }
	public static Double toDouble(final Object object, final double defaultValue){
        if (object == null) {
            return defaultValue;
        }
        if(object instanceof Number){
        	return ((Number)object).doubleValue();
        }
        try {
            return Double.parseDouble(object.toString());
        } catch (NumberFormatException nfe) {
            return defaultValue;
        }
    }
	public static Double toDouble(final Object object){
        if (object == null) {
        	return null;
        }
        if(object instanceof Number){
        	return ((Number)object).doubleValue();
        }
        return Double.parseDouble(object.toString());
    }
	
	public static BigDecimal getBigDecimal(final Object object){
        if (object == null) {
            return null;
        }
        if(object instanceof BigDecimal){
        	return (BigDecimal)object;
        }
        try {
        	return new BigDecimal(object.toString());
        } catch (Exception nfe) {
        	return null;
        }
    }
	public static BigDecimal getBigDecimal(final Object object,BigDecimal defaultValue){
        if (object == null) {
            return defaultValue;
        }
        if(object instanceof BigDecimal){
        	return (BigDecimal)object;
        }
        try {
        	return new BigDecimal(object.toString());
        } catch (Exception nfe) {
        	return defaultValue;
        }
    }
	
	private static BigDecimal unit=new BigDecimal(100);
	/**
	 * 分转成元
	 * @param fen
	 * @return
	 */
	public static BigDecimal fenToYuan(Object fen) {
		if (fen == null) {
            return null;
        }
		BigDecimal b=null;
        if(fen instanceof BigDecimal){
        	b = (BigDecimal)fen;
        }else if(fen instanceof String) {
        	String _fen=(String)fen;
        	if(_fen==null||_fen.length()==0) {
        		return null;
        	}
        	b=new BigDecimal(_fen);
        }else {
        	b=new BigDecimal(fen.toString());
        }
		return b.divide(unit).setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	
	public static int yuanToFen(Object yuan){
		if (yuan == null) {
            return 0;
        }
		BigDecimal b=null;
        if(yuan instanceof BigDecimal){
        	b = (BigDecimal)yuan;
        }else if(yuan instanceof String) {
        	String _yuan=(String)yuan;
        	if(_yuan==null||_yuan.length()==0) {
        		return 0;
        	}
        	b=new BigDecimal(_yuan);
        }else {
        	b=new BigDecimal(yuan.toString());
        }
		return b.multiply(unit).intValue();
	}
	
	
	public static Integer getInteger(final Object obj) {
        Number answer = getNumber(obj);
        if (answer == null) {
            return null;
        } else if (answer instanceof Integer) {
            return (Integer) answer;
        }
        return new Integer(answer.intValue());
    }
    
	public static Number getNumber(final Object answer) {
        if (answer != null) {
            if (answer instanceof Number) {
                return (Number) answer;
                
            } else if (answer instanceof String) {
            	String text = (String) answer;
            	if(text.length()==0) {
            		return null;
            	}
                try {
                    return NumberFormat.getInstance().parse(text);
                    
                } catch (ParseException e) {
                    // failure means null is returned
                }
            }
        }
        return null;
    }
	
}
