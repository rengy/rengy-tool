package cn.rengy.tool.core;

public interface TokenHandler {
	String handleToken(String content);
}

