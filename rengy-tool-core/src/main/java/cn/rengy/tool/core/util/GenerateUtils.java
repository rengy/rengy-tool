package cn.rengy.tool.core.util;

import java.util.Date;

import cn.rengy.tool.core.util._DateUtils;

public class GenerateUtils {
	
	public static Long generateOrderNumber(){
		String time=_DateUtils.formatDate(new Date(), null, "yyMMddHHmmssSSS");
		Double d=Math.random();
		String ds=d.toString();
		return Long.parseLong(time.concat(ds.substring(2,6)));
	}
	
	public static void main(String args[]){
		Double d=Math.random();
		String s=d.toString();
		String time=_DateUtils.formatDate(new Date(), null, "yyMMddHHmmssSSS");
		System.out.println(time);
		System.out.println(Long.MAX_VALUE);
		System.out.println(generateOrderNumber());
	}
}
